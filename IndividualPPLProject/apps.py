from django.apps import AppConfig


class IndividualpplprojectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'IndividualPPLProject'
