from django.urls import path
from IndividualPPLProject.views import show_helloWorld

app_name = 'IndividualPPLProject'

urlpatterns = [
    path('', show_helloWorld, name='show_helloWorld'),

]