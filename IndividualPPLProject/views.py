from django.shortcuts import render
from IndividualPPLProject.models import people

def show_helloWorld(request):
    textFromPsql = people.objects.all().first().Identity
    context = {'text': textFromPsql}
    return render(request, "helloworld.html", context)
